export default {
  index: 'storelist/INDEX',
  list: 'storelist/LIST',
  detail: 'storelist/DETAIL',
  edit: 'storelist/EDIT',
  add: 'storelist/ADD',
  barcodePreview: 'storeList/BARCODE_PREVIEW',
};
