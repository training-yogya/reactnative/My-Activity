import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  logoWrapper: {
    padding: 20,
    width: '100%',
    backgroundColor: 'orange',
  },
  logo: {
    alignSelf: 'flex-start',
    width: 175,
    height: 175,
  },
});
