import React from 'react';
import {Controller, useForm} from 'react-hook-form';
import {AppButton, AppTextInput} from '../../../components';

export default function StoreAddForm({onSubmit, loading}) {
  const {errors, handleSubmit, control} = useForm();
  return (
    <>
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'Name'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.name}
            errorText={errors?.name?.message}
          />
        )}
        name="name"
        rules={{
          required: {value: true, message: 'name is required'},
        }}
        defaultValue={''}
      />
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'Address'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.address}
            errorText={errors?.address?.message}
          />
        )}
        name="address"
        rules={{
          required: {value: true, message: 'address is required'},
        }}
        defaultValue={''}
      />
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'City'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.city}
            errorText={errors?.city?.message}
          />
        )}
        name="city"
        rules={{
          required: {value: true, message: 'city is required'},
        }}
        defaultValue={''}
      />
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'Initial Store'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.initial}
            errorText={errors?.initial?.message}
          />
        )}
        name="initial"
        rules={{
          required: {value: true, message: 'initial is required'},
        }}
        defaultValue={''}
      />
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'Store Code'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.store_code}
            errorText={errors?.store_code?.message}
          />
        )}
        name="store_code"
        rules={{
          required: {value: true, message: 'store_code is required'},
        }}
        defaultValue={''}
      />
      <AppButton
        loading={loading}
        disabled={loading}
        mode="contained"
        onPress={handleSubmit(onSubmit)}>
        Add Store
      </AppButton>
    </>
  );
}
