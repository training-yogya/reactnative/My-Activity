import React, {useState} from 'react';
import {Alert} from 'react-native';
import {AppContainer} from '../../components';
import screenNames from '../../features/StoreList/navigation/screenNames';
import {axiosClient} from '../../utils/axios';

import StoreAddForm from './components/StoreAddForm';

export default function Screen({navigation, route}) {
  const [loading, setLoading] = useState(false);

  // const onSubmit = ({name, address, city}) => {
  const onSubmit = (form) => {
    setLoading(true);

    const data = new FormData();

    // data.append('name', name);
    // data.append('address', address);
    // data.append('city', city);

    Object.keys(form).forEach((formkey) => {
      data.append(formkey, form[formkey]);
    });

    axiosClient
      .post('/store/', data)
      .then((response) => {
        const message = response.data.status.message;
        Alert.alert('Berhasil', message, [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate(screenNames.list, {
                refresh: true,
              });
            },
          },
        ]);
      })
      .catch((error) => {
        const message = error?.response?.data?.status?.message ?? 'Gagal';
        Alert.alert('Error', message);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <AppContainer>
      <StoreAddForm onSubmit={onSubmit} loading={loading} />
    </AppContainer>
  );
}
