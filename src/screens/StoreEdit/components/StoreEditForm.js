import React from 'react';
import {Controller, useForm} from 'react-hook-form';
import {AppButton, AppTextInput} from '../../../components';

export default function StoreEditForm({onSubmit, loading, defaultValue}) {
  const {errors, handleSubmit, control} = useForm();
  return (
    <>
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'Name'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.name}
            errorText={errors?.name?.message}
          />
        )}
        name="name"
        rules={{
          required: {value: true, message: 'name is required'},
        }}
        defaultValue={defaultValue.name}
      />
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'Address'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.address}
            errorText={errors?.address?.message}
          />
        )}
        name="address"
        rules={{
          required: {value: true, message: 'address is required'},
        }}
        defaultValue={defaultValue.address}
      />
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <AppTextInput
            label={'City'}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            error={!!errors.city}
            errorText={errors?.city?.message}
          />
        )}
        name="city"
        rules={{
          required: {value: true, message: 'city is required'},
        }}
        defaultValue={defaultValue.city}
      />
      <AppButton
        loading={loading}
        disabled={loading}
        mode="contained"
        onPress={handleSubmit(onSubmit)}>
        Edit Store
      </AppButton>
    </>
  );
}
