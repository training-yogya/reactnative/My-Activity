import React, {useState} from 'react';
import {Alert} from 'react-native';
import {AppContainer} from '../../components';
import screenNames from '../../features/StoreList/navigation/screenNames';
import {axiosClient} from '../../utils/axios';

import StoreEditForm from './components/StoreEditForm';

export default function Screen({navigation, route}) {
  const storeData = route.params.storeData;

  const [loading, setLoading] = useState(false);

  // const onSubmit = ({name, address, city}) => {
  const onSubmit = (form) => {
    setLoading(true);

    const data = new FormData();

    // data.append('name', name);
    // data.append('address', address);
    // data.append('city', city);

    Object.keys(form).forEach((formkey) => {
      data.append(formkey, form[formkey]);
    });

    axiosClient
      .put('/store/' + storeData.id, data)
      .then((response) => {
        Alert.alert('Berhasil Edit data', '', [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate(screenNames.detail, {
                edited: true,
                storeData: {
                  ...storeData,
                  ...form,
                },
              });
            },
          },
        ]);
      })
      .catch((error) => {
        Alert.alert('Error');
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <AppContainer>
      <StoreEditForm
        onSubmit={onSubmit}
        loading={loading}
        defaultValue={storeData}
      />
    </AppContainer>
  );
}
