import React, {useLayoutEffect, useState, useEffect, useCallback} from 'react';
import {Alert, FlatList} from 'react-native';
import {FAB, IconButton, List} from 'react-native-paper';
import {AppContainer, AppSearchForm} from '../../components';
import {keyExtractor, refreshControl} from '../../utils/flatlist';
import {screenSizes} from '../../utils/style';

import {axiosClient} from '../../utils/axios';
import screenNames from '../../features/StoreList/navigation/screenNames';

export default function Screen({route, navigation}) {
  const [searchResult, setSearchResult] = useState([]);
  const [searchText, setSearchText] = useState('');

  const [loading, setLoading] = useState(false);
  const [storeList, setStoreList] = useState(false);

  useEffect(() => {
    getStore();
  }, []);

  useEffect(() => {
    const refresh = route?.params?.refresh;

    if (refresh) {
      getStore();
      navigation.setParams({
        refresh: false,
      });

      setSearchText('');
    }
  }, [navigation, route.params.refresh]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton icon="plus" size={32} onPress={onLinkToAdd} color="white" />
      ),
    });
  }, [navigation, onLinkToAdd]);

  const getStore = () => {
    setLoading(true);
    axiosClient
      .get('/store')
      .then((response) => {
        const data = response.data.data;

        setStoreList(data);
      })
      .catch((error) => {
        Alert.alert('Error');
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const onLinkToAdd = useCallback(() => {
    navigation.navigate(screenNames.add);
  }, [navigation]);

  return (
    <>
      <AppContainer
        containerStyle={{
          paddingHorizontal: 0,
        }}>
        <AppSearchForm
          setSearchResult={setSearchResult}
          setSearchText={setSearchText}
          searchText={searchText}
          searchFields={['name', 'initial']}
          placeholder="Search Store"
          list={storeList}
        />
        <FlatList
          data={searchText.length > 0 ? searchResult : storeList}
          keyExtractor={keyExtractor('store')}
          refreshControl={refreshControl(loading, getStore)}
          renderItem={({item}) => (
            <List.Item
              style={{width: screenSizes.width}}
              title={item.name}
              onPress={() => {
                navigation.navigate(screenNames.detail, {
                  storeData: item,
                });
              }}
              description={item.initial}
              left={() => <IconButton icon="store" size={32} />}
            />
          )}
        />
      </AppContainer>
      <FAB
        onPress={onLinkToAdd}
        icon="plus"
        style={{
          position: 'absolute',
          bottom: 10,
          right: 10,
        }}
      />
    </>
  );
}
