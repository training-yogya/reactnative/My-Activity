import axios from 'axios';
import {NetworkInfo} from 'react-native-network-info';
import config from '../config';
import {getMainApi} from '../features/Auth/redux/getters';
import {store} from '../redux/store';

import Geolocation from '@react-native-community/geolocation';

const getCoordinate = () => {
  return new Promise((resolve) => {
    Geolocation.getCurrentPosition((position) => {
      const coordinate = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      };

      return resolve(coordinate);
    });
  });
};

const client = axios.create({
  baseURL: config.baseURL,
  headers: {
    ip: null,
    origin: 'yogyagroup.com',
    source: 'sails-api',
    'Content-Type': 'multipart/form-data',
    Authorization: null,
  },
});

client.interceptors.request.use(
  async (axiosOriginalConfig) => {
    const {token} = store.getState().auth;
    const mainApi = getMainApi(store.getState()) || config.baseURL;

    axiosOriginalConfig.baseURL = mainApi;

    if (token) {
      axiosOriginalConfig.headers.Authorization = `Bearer ${token}`;
    }
    const ipAddress = await NetworkInfo.getIPAddress();
    try {
      const coordinate = await getCoordinate();
      axiosOriginalConfig.headers.coordinateString = JSON.stringify(coordinate);
    } catch (error) {
      console.log(error);
    }

    axiosOriginalConfig.headers.ip = ipAddress;

    return axiosOriginalConfig;
  },
  (error) => {
    return Promise.reject(error);
  },
);

export const axiosClient = client;
export const axiosIntance = axios;
